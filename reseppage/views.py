from django.shortcuts import render
from .forms import *
from .models import *

# Create your views here.
def kategori(request):
    kategori = listKategori.objects.all()
    kategoris= formKategori(request.POST or None)
    context = {
        'kategori' : kategori,
        'kategoris' : kategoris
    }
    if request.method == 'POST' :
        if kategoris.is_valid():
            kategori.create(
                namaKategori = kategoris.cleaned_data.get('namaKategori')
            )
    return render(request, 'kategori.html', context)

def resep(request, resep_id) :
    kategori= listKategori.objects.get(id=resep_id)
    resep = listResep.objects.all()
    form_resep = formResep(request.POST or None)
    context = {
        'kategori':kategori,
		'form_resep':form_resep,
		'resep':resep,
    }
    if request.method == 'POST' :
        if form_resep.is_valid():
            resep.create(
                namaResep=form_resep.cleaned_data.get('namaResep'),
                url = form_resep.cleaned_data.get('url'),
                kategori=kategori
            )
    return render(request, 'resep.html', context)

def lihat(request):
    return render(request, 'lihat.html')