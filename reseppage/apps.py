from django.apps import AppConfig


class ReseppageConfig(AppConfig):
    name = 'reseppage'
