from django import forms
from .models import listKategori,listResep

class formKategori(forms.Form):
    class Meta:
        model = listKategori
        fields = "__all__"

    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Maks 30 karakter'
    }
    
    namaKategori = forms.CharField(label = 'Nama Kategori Resep', required=True,
        max_length=30, widget=forms.TextInput(attrs=input_attrs))

class formResep(forms.Form):
    class Meta:
        model = listResep
        fields = "__all__"
    
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Maks 30 karakter'
    }

    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Masukkan URL resep'
    }

    namaResep = forms.CharField(label = 'Nama Resep', required=True,
        max_length=30, widget=forms.TextInput(attrs=input_attrs))
    url = forms.CharField(label = 'URL', required=True,
        max_length=100, widget=forms.TextInput(attrs=input_attrs1))


