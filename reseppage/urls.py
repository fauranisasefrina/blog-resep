from django.urls import path

from . import views

app_name = 'reseppage'

urlpatterns = [
    path('tambahkategoriresep/', views.kategori, name='kategori'),
    path('resep/<int:resep_id>', views.resep, name='resep'),
    path('lihatresep/', views.lihat, name='lihat'),
]
