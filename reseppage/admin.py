from django.contrib import admin
from .models import listKategori, listResep

# Register your models here.

admin.site.register(listKategori)
admin.site.register(listResep)