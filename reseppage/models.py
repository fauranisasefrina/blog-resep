from django.db import models

# Create your models here.
class listKategori(models.Model):
    namaKategori = models.CharField(max_length=300)

class listResep(models.Model):
    namaResep = models.CharField(max_length=30)
    url = models.CharField(max_length= 100, default = True)
    kategori = models.ForeignKey(listKategori, on_delete=models.CASCADE, default = True)